package gui;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;

import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.awt.event.ActionEvent;
import javax.swing.LayoutStyle.ComponentPlacement;
import java.awt.Color;
import javax.swing.UIManager;

public class AtributosControl extends JFrame {

	public JPanel contentPane;
	private JTable table;
	private DefaultTableModel modelo;
	private JScrollPane scrollPane;
	/**
	 * Create the frame.
	 */
	Connection conexion = null;
	PreparedStatement preparedStatement = null;
	ResultSet resultSet = null;
	ResultSetMetaData metaDatos = null;

	public void asignarTablaBD() {
		//conexion e inicializacion de la tabla
				try {
					conexion = Conexion.conectar();

					preparedStatement = conexion
							.prepareStatement("SELECT * FROM atributosControl");
					resultSet = preparedStatement.executeQuery();
					
					metaDatos = resultSet.getMetaData();

					int numeroColumnas = metaDatos.getColumnCount();
					
					modelo = new DefaultTableModel();
					table = new JTable(modelo);
					

				    
					for (int i = 0; i < numeroColumnas; i++)
					{
					   modelo.addColumn( metaDatos.getColumnLabel(i + 1));
					}
					
				
					modelo.addColumn("Editar");
					modelo.addColumn("Eliminar");
					
					numeroColumnas+=2;
					
					Object [] fila;
					int j=0;
					while (resultSet.next())
					{
					   fila = new Object[numeroColumnas]; 

					   for (int i=0;i<numeroColumnas-2;i++)
					      fila[i] = resultSet.getObject(i+1);
					   
					   fila[numeroColumnas-2] = fila[0];
					   fila[numeroColumnas-1] = fila[0];

					   modelo.addRow(fila);
					   j++;
					}
					fila = new Object[numeroColumnas];
					//for add element 
					table.getColumn("Editar").setCellRenderer(new ButtonRenderer());
				    table.getColumn("Editar").setCellEditor(new ButtonEditor(scrollPane,modelo,new JCheckBox()));

					table.getColumn("Eliminar").setCellRenderer(new ButtonRenderer());
				    table.getColumn("Eliminar").setCellEditor(new ButtonEditor(scrollPane,modelo,new JCheckBox()));
				  
					modelo.addRow(fila);

					table.setModel(modelo);
					
					conexion.close();
				}catch(SQLException e) {
					JOptionPane.showMessageDialog(null,"ERROR::CONEXION_BASE_DE_DATOS::"+ e.getMessage());
				}
	}
	public AtributosControl() {
		
		scrollPane = new JScrollPane();
		
		asignarTablaBD();

		///////////////////////////////////////////////
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(0, 0, (int)dim.getWidth()-400,(int)dim.getHeight()-100);
		setTitle("Sistema de Control y Produccion - Atributos");
		contentPane = new JPanel();
		setContentPane(contentPane);
		

		
		JButton btnNewButton = new JButton("Aceptar");
		
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				conexion = Conexion.conectar();

				String fila ; 	
					
					 fila= "(";
					 
					 for (int j = 0; j < modelo.getColumnCount()-2; j++) {
						   if(j==0) {
							   fila += modelo.getValueAt(modelo.getRowCount()-1, j);

						   }
						   else if(j== 2){
							   fila += "," + modelo.getValueAt(modelo.getRowCount()-1, j) ;
						   }
						   else{
							   fila += ","+ "\'" + modelo.getValueAt(modelo.getRowCount()-1, j) + "\'";
						   }
					 }
					 
					 fila+= ")";
					JOptionPane.showMessageDialog(null,fila);

					 try{
						conexion = Conexion.conectar();
						
						preparedStatement = conexion
								.prepareStatement("INSERT INTO atributosControl VALUES " + fila);
						
						preparedStatement.executeUpdate();
						
						conexion.close();

					 }catch(SQLException e) {
					}
					asignarTablaBD(); 
					scrollPane.setViewportView(table);
			}
		});
		
		JButton btnNewButton_2 = new JButton("Cancelar");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				asignarTablaBD();
				scrollPane.setViewportView(table);
			}
		});
		
		
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addComponent(btnNewButton, GroupLayout.PREFERRED_SIZE, 191, GroupLayout.PREFERRED_SIZE)
					.addGap(1139)
					.addComponent(btnNewButton_2, GroupLayout.DEFAULT_SIZE, 174, Short.MAX_VALUE))
				.addComponent(scrollPane, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 1504, Short.MAX_VALUE)
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 843, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnNewButton_2, GroupLayout.PREFERRED_SIZE, 42, GroupLayout.PREFERRED_SIZE)
						.addComponent(btnNewButton, GroupLayout.PREFERRED_SIZE, 41, GroupLayout.PREFERRED_SIZE))
					.addContainerGap())
		);
		

		scrollPane.setViewportView(table);
		contentPane.setLayout(gl_contentPane);
	}
}
