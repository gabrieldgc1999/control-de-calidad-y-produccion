package gui;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Conexion {
	private  static String url = "jdbc:mysql://localhost:3306/Machay";
	private  static String usuario = "root";
	private  static String password = "UNSA";

	public static Connection conectar() {
		Connection conexion=null;
		try {
			conexion = DriverManager.getConnection(url,usuario,password);
		}catch(SQLException e) {
			System.out.println("ERROR::CONEXION_BASE_DE_DATOS");
		}
		return conexion;
	}
}
