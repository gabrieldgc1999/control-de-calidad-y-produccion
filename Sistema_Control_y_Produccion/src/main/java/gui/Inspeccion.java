package gui;

import java.awt.EventQueue;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.Color;
import java.awt.Desktop;
import java.awt.Dimension;

import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;
import javax.swing.JTabbedPane;
import java.awt.CardLayout;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.io.File;
import java.awt.event.ActionEvent;

public class Inspeccion extends JFrame {


	public JPanel contentPane;

	
	private JFrame frmSistemaDeControl;
	private JTextField txtPlaceholder;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	private JTextField textField_5;
	private JLabel lblNewLabel_8;
	private JLabel lblNewLabel_9;
	private JTextField textField_7;
	private JTextField txtC;
	private JTextField textField_9;
	private JLabel lblNewLabel_10;
	private JLabel lblNewLabel_11;
	private JTextField textField_10;
	private JLabel lblNewLabel_12;
	private JTextField textField_11;
	private JTextField textField_12;
	private JLabel lblNewLabel_13;
	private JTable table;
	private JTable table1;
	private JTextField txtCaja;

	/**
	 * Initialize the contents of the frame.
	 */
	public Inspeccion() {
		
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("Sistema de Control y Produccion - Inspecci�n");
		setBounds(0, 0, (int)dim.getWidth()-400,(int)dim.getHeight()-100);
		
		contentPane = new JPanel();
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblNewLabel = new JLabel("Bodega Origen");
		lblNewLabel.setBounds(12, 15, 150, 15);
		getContentPane().add(lblNewLabel);
		
		txtPlaceholder = new JTextField();
		txtPlaceholder.setBounds(161, 15, 114, 19);
		getContentPane().add(txtPlaceholder);
		txtPlaceholder.setColumns(10);
		
		textField = new JTextField();
		textField.setColumns(10);
		textField.setBounds(161, 35, 114, 19);
		getContentPane().add(textField);
		
		txtCaja = new JTextField();
		txtCaja.setBounds(286, 35, 133, 20);
		contentPane.add(txtCaja);
		txtCaja.setColumns(10);
		
		JLabel lblNewLabel_1 = new JLabel("Producto");
		lblNewLabel_1.setBounds(12, 37, 150, 15);
		getContentPane().add(lblNewLabel_1);
		
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(161, 57, 114, 19);
		getContentPane().add(textField_1);
		
		JLabel lblNewLabel_2 = new JLabel("Lote");
		lblNewLabel_2.setBounds(12, 59, 150, 15);
		getContentPane().add(lblNewLabel_2);
		
		textField_2 = new JTextField();
		textField_2.setColumns(10);
		textField_2.setBounds(161, 81, 114, 19);
		getContentPane().add(textField_2);
		
		JLabel lblNewLabel_3 = new JLabel("Cantidad producto");
		lblNewLabel_3.setBounds(12, 83, 150, 15);
		getContentPane().add(lblNewLabel_3);
		
		textField_3 = new JTextField();
		textField_3.setColumns(10);
		textField_3.setBounds(161, 103, 114, 19);
		getContentPane().add(textField_3);
		
		JLabel lblNewLabel_4 = new JLabel("N�mero de bultos");
		lblNewLabel_4.setBounds(12, 105, 150, 15);
		getContentPane().add(lblNewLabel_4);
		
		textField_4 = new JTextField();
		textField_4.setHorizontalAlignment(SwingConstants.LEFT);
		textField_4.setColumns(10);
		textField_4.setBounds(161, 125, 114, 19);
		getContentPane().add(textField_4);
		
		JLabel lblNewLabel_5 = new JLabel("Bultos a Muestrear");
		lblNewLabel_5.setBounds(12, 127, 150, 15);
		getContentPane().add(lblNewLabel_5);
		
		textField_5 = new JTextField();
		textField_5.setColumns(10);
		textField_5.setBounds(161, 147, 114, 19);
		getContentPane().add(textField_5);
		
		JLabel lblNewLabel_6 = new JLabel("Traslado");
		lblNewLabel_6.setBounds(12, 149, 150, 15);
	    getContentPane().add(lblNewLabel_6);
		
		lblNewLabel_8 = new JLabel("Proyecto");
		lblNewLabel_8.setBounds(921, 127, 149, 15);
		getContentPane().add(lblNewLabel_8);
		
		lblNewLabel_9 = new JLabel("Centro de costo");
		lblNewLabel_9.setBounds(921, 105, 149, 15);
		getContentPane().add(lblNewLabel_9);
		
		textField_7 = new JTextField();
		textField_7.setColumns(10);
		textField_7.setBounds(1092, 125, 114, 19);
		getContentPane().add(textField_7);
		
		txtC = new JTextField();
		txtC.setColumns(10);
		txtC.setBounds(1092, 103, 114, 19);
		getContentPane().add(txtC);
		
		textField_9 = new JTextField();
		textField_9.setColumns(10);
		textField_9.setBounds(1092, 81, 114, 19);
		getContentPane().add(textField_9);
		
		lblNewLabel_10 = new JLabel("Fecha Control Ant.");
		lblNewLabel_10.setBounds(921, 83, 150, 15);
		getContentPane().add(lblNewLabel_10);
		
		lblNewLabel_11 = new JLabel("Inspecciones realizada");
		lblNewLabel_11.setBounds(921, 59, 173, 15);
		getContentPane().add(lblNewLabel_11);
		
		textField_10 = new JTextField();
		textField_10.setText("0");
		textField_10.setColumns(10);
		textField_10.setBounds(1092, 57, 114, 19);
		getContentPane().add(textField_10);
		
		lblNewLabel_12 = new JLabel("Fecha Control");
		lblNewLabel_12.setBounds(921, 37, 149, 15);
		getContentPane().add(lblNewLabel_12);
		
		textField_11 = new JTextField();
		textField_11.setToolTipText("");
		textField_11.setColumns(10);
		textField_11.setBounds(1092, 35, 114, 19);
		getContentPane().add(textField_11);
		
		textField_12 = new JTextField();
		textField_12.setText("123");
		textField_12.setColumns(10);
		textField_12.setBounds(1092, 15, 114, 19);
		getContentPane().add(textField_12);
		
		lblNewLabel_13 = new JLabel("No Doc.");
		lblNewLabel_13.setBounds(921, 15, 149, 15);
		getContentPane().add(lblNewLabel_13);
		
		JButton btnNewButton = new JButton("O");
		btnNewButton.setBackground(new Color(218, 165, 32));
		btnNewButton.setBounds(276, 15, 45, 19);
		getContentPane().add(btnNewButton);
		
		String units[]= { "Est Glenol x 10 Caps.", "placeholder"};
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(40, 187, 1313, 458);
		contentPane.add(tabbedPane);
		

		


		JPanel panel_1 = new JPanel();
		tabbedPane.addTab("Cuantitativo", null, panel_1, null);
		panel_1.setLayout(new CardLayout(0, 0));
		
		JScrollPane scrollPane = new JScrollPane();
		panel_1.add(scrollPane, "name_171260543809600");
		
		table = new JTable();
		table.setModel(new DefaultTableModel(
			new Object[][] {
				{null, null, null, null, null},
				{null, null, null, null, null},
				{null, null, null, null, null},
				{null, null, null, null, null},
				{null, null, null, null, null},
				{null, null, null, null, null},
			},
			new String[] {
				"Codigo Atr.", "Atributo", "Critico", "Aprobado", "Tama�o de muestra"
			}
		));
		scrollPane.setViewportView(table);
		
		JPanel panel = new JPanel();
		tabbedPane.addTab("Cualitativo", null, panel, null);
		panel.setLayout(new CardLayout(0,0));
		
		JScrollPane scrollPane1 = new JScrollPane();
		panel.add(scrollPane1, "name_171260543809601");
		
		table1 = new JTable();
		table1.setModel(new DefaultTableModel(
			new Object[][] {
				{null, null, null, null, null},
				{null, null, null, null, null},
				{null, null, null, null, null},
				{null, null, null, null, null},
				{null, null, null, null, null},
				{null, null, null, null, null},
				{null, null, null, null, null},
			},
			new String[] {
				"Atributo", "Descripcion Atributo", "Un. Medida", "Critico", "Equipo"
			}
		));
		scrollPane1.setViewportView(table1);
		
		JButton btnNewButton_1 = new JButton("Buscar");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				txtPlaceholder.setText("ALPE-C");
				textField.setText("10001");
				txtCaja.setText("caja cerveza loba x6 un.");
				textField_4.setText("8");
				textField_11.setText("25/09/2019");
				textField_2.setText("100");
				textField_1.setText("501");
				textField_3.setText("50");
				txtC.setText("Control de calidad");
				table.setModel(new DefaultTableModel(
						new Object[][] {
							{993, "Indice de alcalinidad", "Si", "Si", "8"},
							{998, "Indice de peroxido", "No", "Si", "8"},
							{994, "Cantidad antiespumante", "No", "Si", "8"},
							{null, null, null, null, null},
							{null, null, null, null, null},
							{null, null, null, null, null},
						},
						new String[] {
							"Codigo Atr.", "Atributo", "Critico", "Aprobado", "Tama�o de muestra"
						}
					));
				table1.setModel(new DefaultTableModel(
						new Object[][] {
							{"Indice de alcalinidad", "Medida de su capacidad para neutralizar �cidos", "ph", "4.1 - 4.6", "ph METRO"},
							{"Indice de peroxido", "Mide el estado de oxidaci�n inicial", "gr/Hl", "-1.50 -1.53", "DENSIMETRO"},
							{"Cantidad antiespumante", "Imprescindible para la ebullici�n de cervezas", "5gr/Hl a 50gr/Hl", "HIDROMETRO"},
							{null, null, null, null, null},
							{null, null, null, null, null},
							{null, null, null, null, null},
							{null, null, null, null, null},
						},
						new String[] {
							"Atributo", "Descripcion Atributo", "Un. Medida", "Critico", "Equipo"
						}
					));

			}
		});
		btnNewButton_1.setBounds(1226, 11, 89, 23);
		contentPane.add(btnNewButton_1);
		
		JButton btnNewButton_2 = new JButton("Certificado");
		btnNewButton_2.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					try  
					{  
						//constructor of file class having file as argument  
						File file = new File("C:/Users/gabri/Downloads/Reporte.pdf");   
						if(!Desktop.isDesktopSupported())//check if Desktop is supported by Platform or not  
						{  
							System.out.println("not supported");  
							return;  
						}  
						Desktop desktop = Desktop.getDesktop();  
						if(file.exists())         //checks file exists or not  
							desktop.open(file);              //opens the specified file  
					}  catch(Exception x)  	{  
						x.printStackTrace();  
						}  
					}  
		});
		btnNewButton_2.setBounds(40, 677, 89, 23);
		contentPane.add(btnNewButton_2);
		
	}
}
