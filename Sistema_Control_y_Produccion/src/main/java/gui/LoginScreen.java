package gui;

import java.awt.Dimension;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.awt.event.ActionEvent;
import java.awt.Font;
import java.awt.Toolkit;

public class LoginScreen extends JFrame{

	public JPanel contentPane;
	public JButton btnIniciarSesion;
	public boolean  signIn = false;
	
	public JTextField userTextField;
	public JPasswordField passwordField;
	
	public Connection conexion = null;
	public PreparedStatement statement = null;
	public ResultSet resultSet = null;

	/**
	 * Initialize the contents of the frame.
	 */
	public LoginScreen() {
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		setEnabled(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(10, 11, 1894, 989);
		setTitle("Inicio de Sesi�n - Machay");
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel userLabel = new JLabel("Usuario:");
		userLabel.setBounds(817, 474, 160, 14);
		getContentPane().add(userLabel);
		
		btnIniciarSesion = new JButton("Iniciar sesi�n");

		btnIniciarSesion.setBounds(1044, 640, 95, 23);
		getContentPane().add(btnIniciarSesion);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(994, 546, 145, 20);
		getContentPane().add(passwordField);
		
		JLabel passwordLabel = new JLabel("Contrase�a:");
		passwordLabel.setBounds(817, 549, 160, 14);
		getContentPane().add(passwordLabel);
		
		JLabel loginLabel = new JLabel("Inicio de Sesi�n - Machay");
		loginLabel.setFont(new Font("FuraCode NF", Font.BOLD, 20));
		loginLabel.setBounds(648, 359, 241, 26);
		getContentPane().add(loginLabel);
		
		userTextField = new JTextField();
		userTextField.setBounds(994, 471, 145, 20);
		getContentPane().add(userTextField);
		userTextField.setColumns(10);
		
	}
}

