package gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.awt.event.ActionEvent;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JLayeredPane;
import javax.swing.JOptionPane;

import java.awt.CardLayout;
import java.awt.Dimension;

import javax.swing.JLabel;
import javax.swing.SwingConstants;

public class MainWindow extends JFrame {

	JLayeredPane layeredPane = new JLayeredPane();
	
	LoginScreen loginScreen = new LoginScreen();
	AtributosControl atributosControl = new AtributosControl();	
	Instrumentacion instrumentacion = new Instrumentacion();
	Inspeccion inspeccion = new Inspeccion();
	SalidaMercancia salidaMercancia = new SalidaMercancia();
	/**
	 * Launch the application.
	 */	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainWindow frame = new MainWindow();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	public void  switchPanels(JLayeredPane layeredPane,JPanel panel) {
		layeredPane.removeAll();
		layeredPane.add(panel);
		layeredPane.repaint();
		layeredPane.revalidate();
	}

	/**
	 * Create the frame.
	 * 
	 */

	public void initFrameLogin() {
		layeredPane.setBounds(10, 11, 1894, 989);

		getContentPane().add(layeredPane);
		layeredPane.setLayout(new CardLayout(0, 0));
		
		
		final JPanel login_panel = loginScreen.contentPane;
		layeredPane.add(login_panel, "name_204580173676401");
		
		switchPanels(layeredPane,loginScreen.contentPane);
		
	}
	
	public void initFrameSystem() {
		layeredPane.setBounds(342, 31, 1504, 930);
		getContentPane().add(layeredPane);
		layeredPane.setLayout(new CardLayout(0, 0));
		
		final JPanel panel = atributosControl.contentPane;
		layeredPane.add(panel, "name_204580173676400");
		
		
		final JPanel panel_1 = instrumentacion.contentPane;
		layeredPane.add(panel_1, "name_204590928921900");

		switchPanels(layeredPane,atributosControl.contentPane);

		JButton btnNewButton = new JButton("Atributos de control");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				switchPanels(layeredPane,atributosControl.contentPane);
			}
		});
		btnNewButton.setBounds(48, 31, 213, 42);
		getContentPane().add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Instrumentacion");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				switchPanels(layeredPane,instrumentacion.contentPane);
			}
		});
		btnNewButton_1.setBounds(48, 104, 213, 42);
		getContentPane().add(btnNewButton_1);
		
		JButton btnNewButton_1_1 = new JButton("Inspeccion");
		btnNewButton_1_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				switchPanels(layeredPane,inspeccion.contentPane);
			}
		});
		btnNewButton_1_1.setBounds(48, 168, 213, 42);
		getContentPane().add(btnNewButton_1_1);
		
		JButton btnNewButton_1_1_1 = new JButton("Salida de Mercancia");
		btnNewButton_1_1_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				switchPanels(layeredPane,salidaMercancia.contentPane);
			}
		});
		btnNewButton_1_1_1.setBounds(48, 243, 213, 42);
		getContentPane().add(btnNewButton_1_1_1);
		
	}
	
	
	public MainWindow() {
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(0, 0, (int)dim.getWidth(),(int)dim.getHeight()-40);
		setTitle("Sistema de Control y Produccion");
		getContentPane().setLayout(null);
		

		initFrameLogin();

		loginScreen.btnIniciarSesion.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String username = loginScreen.userTextField.getText();
				String password = loginScreen.passwordField.getText();
				if (!(username.isBlank()) && !(password.isBlank())) {
					// System.out.println(userTextField.getText());
					// System.out.println(passwordField.getPassword());
					loginScreen.conexion = Conexion.conectar();
					try {
						loginScreen.statement = loginScreen.conexion.prepareStatement("CALL `Machay`.`checkUser`(?, ?)");
						loginScreen.statement.setString(1, username);
						loginScreen.statement.setString(2, password);
						loginScreen.resultSet = loginScreen.statement.executeQuery();
						loginScreen.resultSet.next();
						String response = loginScreen.resultSet.getString(1);
						if (response.equals("0")) {
							JOptionPane.showMessageDialog(null, "Username o password equivocado");
						} else {
							System.out.println(response);
							initFrameSystem();						}
					} catch (SQLException error) {
						System.out.println(error);
					}
				} else {
					JOptionPane.showMessageDialog(null, "Username o password vac�o");
				}
			}
		});


	}
}
