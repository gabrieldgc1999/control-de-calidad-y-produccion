package gui;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

import javax.swing.DefaultCellEditor;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;



class ButtonRenderer extends JButton implements TableCellRenderer {
  
	
  public ButtonRenderer() {
    setOpaque(true);
  }
  
  public Component getTableCellRendererComponent(JTable table, Object value,
      boolean isSelected, boolean hasFocus, int row, int column) {
    if (isSelected) {
      setForeground(table.getSelectionForeground());
      setBackground(table.getSelectionBackground());
    } else {
      setForeground(table.getForeground());
      setBackground(UIManager.getColor("Button.background"));
    }
    setText((value == null) ? "" : value.toString());
    return this;
  }
}

class ButtonEditor extends DefaultCellEditor {
  protected JButton button;

  private String label;
  private String type;
  private int rowtable;
  
  private JTable jtable;
  private DefaultTableModel dmodelo;
  private JScrollPane scrollPane;
  private boolean isPushed;
  
  Connection conexion = null;
  PreparedStatement preparedStatement = null;
  ResultSet resultSet = null;
  ResultSetMetaData metaDatos = null;
  
  public ButtonEditor(JScrollPane scrollPan,DefaultTableModel modelo,JCheckBox checkBox) {
    super(checkBox);
    dmodelo = modelo;
    button = new JButton();
    button.setOpaque(true);
    scrollPane = scrollPan;
    button.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {	  
        fireEditingStopped();
      }
    });
  }

  public Component getTableCellEditorComponent(JTable table, Object value,
      boolean isSelected, int row, int column) {
    if (isSelected) {
      button.setForeground(table.getSelectionForeground());
      button.setBackground(table.getSelectionBackground());
    } else {
      button.setForeground(table.getForeground());
      button.setBackground(table.getBackground());
    }
    
    jtable = table;
    label = (value == null) ? "" : value.toString();
    type = table.getColumnName(column); 
    rowtable = row;
    button.setText(label);
    isPushed = true;
    
    return button;
  }

  public Object getCellEditorValue() {
    if (isPushed) {
    	JOptionPane.showMessageDialog(button,type+label);
    }
    isPushed = false;
    return new String(label);
  }

  public boolean stopCellEditing() {
    isPushed = false;
    return super.stopCellEditing();
  }

  protected void fireEditingStopped() {
    super.fireEditingStopped();
	String fila ; 	
	
	 fila= "(";
	 
	 for (int j = 0; j < jtable.getColumnCount()-2; j++) {
		   if(j==0) {
			   fila += jtable.getValueAt(rowtable, j);

		   }
		   else if(j== 2){
			   fila += "," + jtable.getValueAt(rowtable, j) ;
		   }
		   else{
			   fila += ","+ "\'" + jtable.getValueAt(rowtable , j) + "\'";
		   }
	 }
	 
	 fila+= ")";
	 JOptionPane.showMessageDialog(null,fila);
	 
	 if(type == "Eliminar") {
		 try{
				conexion = Conexion.conectar();
				
				preparedStatement = conexion
						.prepareStatement("DELETE FROM atributosControl WHERE atributo = " + jtable.getValueAt(rowtable, 0));
				
				preparedStatement.executeUpdate();
				
				
				conexion.close();

				
			 }catch(SQLException e) {
				JOptionPane.showMessageDialog(null,"ERROR::CONEXION_BASE_DE_DATOS::"+ e.getMessage());
			}
			asignarTablaBD(); 
			scrollPane.setViewportView(jtable);

	 }
	 else if(type == "Editar") {
		 try{
				conexion = Conexion.conectar();
				
				preparedStatement = conexion
						.prepareStatement("DELETE FROM atributosControl WHERE atributo = " + jtable.getValueAt(rowtable, 0));
				
				preparedStatement.executeUpdate();
				
				preparedStatement = conexion
						.prepareStatement("INSERT INTO atributosControl VALUES " + fila);
				
				preparedStatement.executeUpdate();
				
				conexion.close();

				
			 }catch(SQLException e) {
				JOptionPane.showMessageDialog(null,"ERROR::CONEXION_BASE_DE_DATOS::"+ e.getMessage());
			}
			asignarTablaBD(); 
			scrollPane.setViewportView(jtable);
		 
	 }
 }
  public void asignarTablaBD() {
		//conexion e inicializacion de la tabla
				try {
					conexion = Conexion.conectar();

					preparedStatement = conexion
							.prepareStatement("SELECT * FROM atributosControl");
					resultSet = preparedStatement.executeQuery();
					
					metaDatos = resultSet.getMetaData();

					int numeroColumnas = metaDatos.getColumnCount();
					
					dmodelo = new DefaultTableModel();
					jtable = new JTable(dmodelo);
					

				    
					for (int i = 0; i < numeroColumnas; i++)
					{
					   dmodelo.addColumn( metaDatos.getColumnLabel(i + 1));
					}
					
				
					dmodelo.addColumn("Editar");
					dmodelo.addColumn("Eliminar");
					
					numeroColumnas+=2;
					
					Object [] fila;
					int j=0;
					while (resultSet.next())
					{
					   fila = new Object[numeroColumnas]; 

					   for (int i=0;i<numeroColumnas-2;i++)
					      fila[i] = resultSet.getObject(i+1);
					   
					   fila[numeroColumnas-2] = fila[0];
					   fila[numeroColumnas-1] = fila[0];

					   dmodelo.addRow(fila);
					   j++;
					}
					fila = new Object[numeroColumnas];
					//for add element 
					jtable.getColumn("Editar").setCellRenderer(new ButtonRenderer());
				    jtable.getColumn("Editar").setCellEditor(new ButtonEditor(scrollPane,dmodelo,new JCheckBox()));

					jtable.getColumn("Eliminar").setCellRenderer(new ButtonRenderer());
				    jtable.getColumn("Eliminar").setCellEditor(new ButtonEditor(scrollPane,dmodelo,new JCheckBox()));
				  
					dmodelo.addRow(fila);

					jtable.setModel(dmodelo);
					
					conexion.close();
				}catch(SQLException e) {
					JOptionPane.showMessageDialog(null,"ERROR::CONEXION_BASE_DE_DATOS::"+ e.getMessage());
				}
	}
}
